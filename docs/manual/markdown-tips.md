

# markdown 指南

- Markdown 是一种轻量级的「标记语言」，它的优点很多，目前也被越来越多的写作爱好者，撰稿者广泛使用。
- 看到这里请不要被「标记」、「语言」所迷惑，Markdown 的语法十分简单。
- 常用的标记符号也不超过十个，这种相对于更为复杂的HTML 标记语言来说，Markdown 可谓是十分轻量的。
- 学习成本也不需要太多，且一旦熟悉这种语法规则，会有一劳永逸的效果。



## 参考教程:

- [Markdown 新手指南](http://www.jianshu.com/p/q81RER)
- [认识与入门 Markdown](http://sspai.com/25137)

