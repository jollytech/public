# 公开参考文档

- 本项目是管理公司日常开发的一些公开文档.
- 禁止上传公司业务相关的重要文档.


## 目录:

- [git-osc 项目权限管理说明]()
    - [git-osc 项目管理权限说明](http://git.oschina.net/jollytech/public/wikis/git-osc-project-user-permission)
- [Markdown]()
    - [Markdwon 指南](./docs/manual/markdown-tips.md)
- [Git](./docs/git)
    - [Git 配置](./docs/git/git-config.md)
    - [Git 常用命令集锦](./docs/git/git-tips.md)
- [Python](./docs/python)
    - [Python 常用开发包](./docs/python/python-packages.md)
    
    
    
    
